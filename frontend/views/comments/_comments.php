<?php
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;

/* @var $this yii\web\View */
/* @var $model frontend\models\Comments */


?>
<div class="post">
    <h4><?= Html::encode($model->fullname) ?></h4>

    <?= HtmlPurifier::process($model->text) ?>
</div>
