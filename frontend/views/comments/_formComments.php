<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model frontend\models\Comments */
/* @var $form yii\widgets\ActiveForm */

$this->registerJs(
    '$("document").ready(function(){ 
        $("#new_comment").on("pjax:end", function() {
            $.pjax.reload({container:"#comments"});
        });
    });'
);
?>

<div class="comments-form">
    <?php
    Pjax::begin(['id' => 'new_comment']);
    $form = ActiveForm::begin([
        'options' => ['data' => ['pjax' => true]],
    ]); ?>

    <?= $form->field($model, 'fullname')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>
    <?= $form->field($model, 'articles_id')->hiddenInput(['value'=> $article_id])->label(false) ?>

    <div class="form-group">
        <?= Html::submitButton('Add a comment!', ['class' => 'btn btn-primary']) ?>
    </div>
    <?php
    ActiveForm::end();
    Pjax::end();
    ?>
</div>
