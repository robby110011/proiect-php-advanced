<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Articles';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="articles-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemOptions' => ['class' => 'item'],
        'itemView' => function ($model, $key, $index, $widget) {
            $itemContent = Html::tag('h4',Html::a(Html::encode($model->title), ['view', 'id' => $model->id]));

            $itemContent .= Html::tag('div',(Html::encode(StringHelper::truncateWords($model->text, 50, '...'))));
            return $itemContent;
        },
    ]) ?>
</div>
