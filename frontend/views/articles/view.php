<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model frontend\models\Articles */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Articles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="articles-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col-md-12"><?= $model->text ?></div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-6">
            <h2>Comments</h2>
            <div id="people-comments">
                <?php Pjax::begin(['id' => 'comments']) ?>
                <?= ListView::widget([
                    'dataProvider' => $dataProvider,
                    'itemView' => '../comments/_comments',
                ]) ?>
                <?php Pjax::end() ?>
            </div>
        </div>
        <div class="col-md-6">
            <h2>Add new comment</h2>
            <?= $this->render('../comments/_formComments', [
                'model' => $comments,
                'article_id' => $model->id,
            ]) ?>
        </div>
    </div>
</div>
