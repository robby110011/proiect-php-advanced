<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "comments".
 *
 * @property integer $id
 * @property integer $articles_id
 * @property string $fullname
 * @property string $text
 *
 * @property Articles $articles
 */
class Comments extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'comments';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['articles_id'], 'integer'],
            [['articles_id'], 'required'],
            [['fullname', 'text'], 'required'],
            [['text'], 'string'],
            [['fullname'], 'string', 'max' => 150],
            [['articles_id'], 'exist', 'skipOnError' => true, 'targetClass' => Articles::className(), 'targetAttribute' => ['articles_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'articles_id' => 'Article',
            'fullname' => 'Fullname',
            'text' => 'Text',
            'articles.title' => 'Article',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticles()
    {
        return $this->hasOne(Articles::className(), ['id' => 'articles_id']);
    }

}
