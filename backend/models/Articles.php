<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "articles".
 *
 * @property integer $id
 * @property string $title
 * @property string $text
 * @property string $active
 */
class Articles extends \yii\db\ActiveRecord
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'articles';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['text', 'active'], 'string'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'text' => 'Text',
            'active' => 'Active',
        ];
    }
    public function getActiveArticles()
    {
        return Articles::find()
            ->where(['active' => $this->STATUS_ACTIVE])
            ->orderBy('id')
            ->all();
//        return "some text";
    }
    public function getComments()
    {
        // a customer has many comments
        return $this->hasMany(Comments::className(), ['articles_id' => 'id']);
    }

}
